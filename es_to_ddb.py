from elasticsearch import Elasticsearch
import boto3
from boto3.dynamodb.conditions import Key, Attr
import json

aws_session = boto3.Session(
    aws_access_key_id="ACCESS_KEY",
    aws_secret_access_key="SECRET",
    region_name="REGION"
)

dynamo_db = aws_session.resource('dynamodb')
table = dynamo_db.Table("TABLE_NAME")

INDEX = "articles"

body = {
    "_source": ["domain", "type", "updated_at", "headline"],
}
doc_type = "_doc"
size = 50

es = None
es = Elasticsearch(
    ['ES_ENDPOINT'])

if es.ping():
    print('Connected')
else:
    print('Could not connect')


def process_hits(hits):
    items = list(
        map(lambda item: {'domain': item['_source']['domain'], 'url_hash': item['_id'], 'article_type': item['_source']['type'], 'headline': item['_source']['headline'], 'crawled_ts': item['_source']['updated_at']}, hits))

    res = None
    with table.batch_writer() as batch:
        for item in items:
            res = batch.put_item(Item=item)

    return None


# Check index exists
if not es.indices.exists(index=INDEX):
    print("Index " + index + " not exists")
    exit()


data = es.search(
    index=INDEX,
    scroll='2m',
    size=size,
    body=body
)

# Get the scroll ID
sid = data['_scroll_id']
scroll_size = len(data['hits']['hits'])

while scroll_size > 0:
    "Scrolling..."

    # Before scroll, process current batch of hits
    process_hits(data['hits']['hits'])

    data = es.scroll(scroll_id=sid, scroll='2m')

    # Update the scroll ID
    sid = data['_scroll_id']

    # Get the number of results that returned in the last scroll
    scroll_size = len(data['hits']['hits'])
