import boto3
import time
from boto3.dynamodb.conditions import Key
from boto3.dynamodb.conditions import Attr
from botocore.config import Config
import sys
import logging

logging.getLogger('boto').setLevel(logging.CRITICAL)


class DynamoDB:

    def __init__(self, table_name, *args, **kwargs):
        aws_session = boto3.Session(
            aws_access_key_id="ACCESS_KEY",
            aws_secret_access_key="SECRET_ACCESS_KEY",
            region_name="REGION_NAME"
        )

        config = Config(
            connect_timeout=2, read_timeout=2,
            retries={'max_attempts': 12})
        self.table_name = table_name
        self.conn = aws_session.resource('dynamodb', config=config)
        self.table = self.conn.Table(table_name)

    def batch_read(self, keys):
        """
        [INFO] Reads data in batches from dynamoDB
        [NOTE] Do not provide more than 100 items per call
        [NOTE] The duplicates are automatically removed
        [WARNING] The data returned is not in any particular order
        [PARAMS]
        @keys: list of keys of all the items that need to be read from the table
        [RETURN] responses obtained from reading the table
        """

        res = self.conn.batch_get_item(
            RequestItems={
                self.table_name: {
                    'Keys': keys
                }
            }
        )

        res_extracted = res.get('Responses').get(self.table_name)
        return res_extracted

    def batch_write(self, items):
        """
        TODO: Document this method
        """
        with self.table.batch_writer() as batch:
            for item in items:
                batch.put_item(Item=item)
        return True

    def insert_item(self, item):
        """
        [INFO] Insert an item to table
        [PARAMS]
        @item: a dictionary with key-value pairs of the information that is to be inserted into the table
        [RETURN] Boolean
        """
        response = self.table.put_item(Item=item)
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
            return False

    def get_item(self, primary_key):
        """
        [INFO] Get an item given its key
        [PARAMS]
        @primary_key: A dictionary with partition key and sort key(optional) as key-value pair
        [RETURN] Item in the Repsonse body
        """
        response = self.table.get_item(
            Key=primary_key
        )
        res_item = response.get('Item')
        return res_item

    def update_item(self, key_dict, update_dict):
        """
        [INFO] Update an item.
        [PARAMS]
        @key_dict: dict containing the key name and val eg. {"uuid": item_uuid}
        @update_dict: dict containing the key name and val of
        attributes to be updated
        eg. {"headline": "this is perfect 123", "timestamp": "2019-12-04"}
        [RETURN] Boolean
        """
        update_expr_items = ''
        expr_attr_val = dict()
        for item in update_dict.items():
            update_expr_items += f' {item[0]}=:{item[0]},'
            expr_attr_val[f':{item[0]}'] = item[1] or 0

        update_expr = f'SET {update_expr_items}'
        response = self.table.update_item(
            Key=key_dict,
            UpdateExpression=update_expr.rstrip(','),
            ExpressionAttributeValues=expr_attr_val
        )
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
            return False

    def query_item(
        self, table_name, sort_key, partition_key,
        index_name=None, total_items=None, start_key=None,
        table=None
    ):
        """
        DO NOT USE - UNDER DEVELOPMENT
        Query for an item with or without using global secondary index
        PARAMS:
        @table_name: name of the table
        @sort_key: Dict containing key and val of sort key
        e.g. {'name': 'uuid', 'value': '077f4450-96ee-4ba8-8faa-831f6350a860'}
        @partition_key: Dict containing key and val of partition key
        e.g. {'name': 'date', 'value': '2017-02-12'}
        @index_name (optional): Name of the Global Secondary Index
        """
        if not table:
            dynamodb = self.conn
            table = dynamodb.Table(table_name)
        sk = sort_key['name']
        skv = sort_key['value']
        pk = partition_key['name']
        pkv = partition_key['value']
        if not start_key:
            if index_name:
                response = table.query(
                    IndexName=index_name,
                    KeyConditionExpression=Key(sk).eq(skv) &
                    Key(pk).eq(pkv)
                )
            else:
                response = table.query(
                    KeyConditionExpression=Key(sk).eq(skv) &
                    Key(pk).eq(pkv)
                )
        else:
            if index_name:
                response = table.query(
                    IndexName=index_name,
                    KeyConditionExpression=Key(sk).eq(skv) &
                    Key(pk).eq(pkv),
                    ExclusiveStartKey=start_key
                )
            else:
                response = table.query(
                    KeyConditionExpression=Key(sk).eq(skv) &
                    Key(pk).eq(pkv),
                    ExclusiveStartKey=start_key
                )
        if not total_items:
            total_items = response['Items']
        else:
            total_items.extend(response['Items'])
        if response.get('LastEvaluatedKey'):
            start_key = response['LastEvaluatedKey']
            return_items = self.query_item(
                table_name=table_name, sort_key=sort_key,
                partition_key=partition_key, total_items=total_items,
                start_key=start_key, table=table
            )
            return return_items
        else:
            return total_items

    def scan_item(
        self, table_name, attr1, attr2,
        total_items=None, start_key=None,
        table=None
    ):
        """
        DO NOT USE - UNDER DEVELOPMENT
        Scan for an item with two attributes
        NOTE: SCAN OPERATION SCANS THE WHOLE TABLE AND TAKES CONSIDERABLE
        AMOUNT OF TIME, CONSUMES HIGH READ THROUGHPUT.
        AVOID USING THIS AS MUCH AS YOU CAN.
        TRY CREATING INDEX AND USE QUERY IF POSSIBLE
        PARAMS:
        @table_name: name of the table
        @attr1: Dict containing key and val of first attribute
        e.g. {'name': 'uuid', 'value': '077f4450-96ee-4ba8-8faa-831f6350a860'}
        @attr2: Dict containing key and val of second attribute
        e.g. {'name': 'date', 'value': '2017-02-12'}
        """
        if not table:
            dynamodb = self.conn
            table = dynamodb.Table(table_name)
        a1 = attr1['name']
        a1v = attr1['value']
        a2 = attr2['name']
        a2v = attr2['value']
        if not start_key:
            response = table.scan_item(
                FilterExpression=Attr(a1).eq(a1v) &
                Attr(a2).eq(a2v)
            )
        else:
            response = table.scan_item(
                FilterExpression=Attr(a1).eq(a1v) &
                Attr(a2).eq(a2v),
                ExclusiveStartKey=start_key
            )
        if not total_items:
            total_items = response['Items']
        else:
            total_items.extend(response['Items'])
        if response.get('LastEvaluatedKey'):
            start_key = response['LastEvaluatedKey']
            return_items = self.query_item(
                table_name=table_name, attr1=attr1,
                attr2=attr2, total_items=total_items,
                start_key=start_key, table=table
            )
            return return_items
        else:
            return total_items

    def delete_item(self, table_name, item_key):
        """
        DO NOT USE - UNDER DEVELOPMENT
        delete an item
        PARAMS
        @table_name: name of the table
        @item_key: dict containing key and val of sort key
        e.g. {'name': 'uuid', 'value': 'some-uuid-val'}
        """
        dynamodb = self.conn
        table = dynamodb.Table(table_name)
        response = table.delete_item(
            Key={item_key['name']: item_key['value']}
        )
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
            return False

    def create_table(
        self, table_name, hash_name,
        read_throughput=5, write_throughput=5
    ):
        """
        DO NOT USE - UNDER DEVELOPMENT
        Create the DynamoDB table.
        NOTE: THIS IS A DEMO TABLE WITH ONLY A HASH KEY of type String.
        """
        dynamodb = self.conn
        table = dynamodb.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': hash_name,
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': hash_name,
                    'AttributeType': 'S'
                },
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': read_throughput,
                'WriteCapacityUnits': write_throughput
            }
        )
        if table:
            print("Success !")
        return table

    def delete_all_items(self, table_name, hash_name):
        """
        DO NOT USE - UNDER DEVELOPMENT
        Delete all items in a table by recreating the table.
        """
        dynamodb = self.conn
        try:
            table = dynamodb.Table(table_name)
            table.delete()
        except:
            print(
                "Error in deletion. Table {} does not exist.".format(
                    table_name))
        # allow time for table deletion
        time.sleep(5)
        try:
            table = self.create_table(table_name, hash_name=hash_name)
        except:
            print("Error in creating table {}".format(table_name))
